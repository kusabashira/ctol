package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

func version() {
	os.Stderr.WriteString(`ctol: v0.3.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: ctol [OPTION] [FILE]...
Convert to line column

Options:
	-splitsep -s    split input field by it
	-joinsep  -j    join output field by it
	--help          show this help message and exit
	--version       print the version and exit

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

func toSplitterReg(sep string) (*regexp.Regexp, error) {
	sep = regexp.QuoteMeta(sep)
	return regexp.Compile(`\s*` + sep + `\s*`)
}

func _main() (exitCode int, err error) {
	var splitSep string
	var joinSep string
	var isHelp, isVersion bool
	flag.StringVar(&splitSep, "s", " ", "split input field by it")
	flag.StringVar(&splitSep, "splitsep", " ", "split input field by it")
	flag.StringVar(&joinSep, "j", " ", "join output field by it")
	flag.StringVar(&joinSep, "joinsep", " ", "join output field by it")
	flag.BoolVar(&isHelp, "help", false, "print the help")
	flag.BoolVar(&isVersion, "version", false, "print the version")
	flag.Usage = usage
	flag.Parse()

	if isHelp {
		usage()
		return 0, nil
	}
	if isVersion {
		version()
		return 0, nil
	}

	var input io.Reader
	if flag.NArg() < 1 {
		input = os.Stdin
	} else {
		var inputs []io.Reader
		for _, name := range flag.Args() {
			in, err := os.Open(name)
			if err != nil {
				return 1, err
			}
			defer in.Close()
			inputs = append(inputs, in)
		}
		input = io.MultiReader(inputs...)
	}

	sep, err := toSplitterReg(splitSep)
	if err != nil {
		return 1, err
	}

	cols := make([][]string, 0, 8)
	reader := bufio.NewScanner(input)
	for reader.Scan() {
		line := reader.Text()
		for nocol, cell := range sep.Split(line, -1) {
			if len(cols) <= nocol {
				cols = append(cols, make([]string, 0, 8))
			}
			cols[nocol] = append(cols[nocol], cell)
		}
	}

	for _, col := range cols {
		fmt.Println(strings.Join(col, joinSep))
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "ctol:", err)
	}
	os.Exit(exitCode)
}
